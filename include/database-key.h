#pragma once

#include <string>
#include <vector>

template <class TClass>
struct IDatabaseKey 
{
	static std::vector<std::string> GetKeys() 
	{
		return TClass::GetCollectionNameInternal();
	}
};
