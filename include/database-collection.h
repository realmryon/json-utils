#pragma once

#include <string>
#include <vector>

template <class TClass>
struct IDatabaseCollection
{

	virtual std::vector<std::string> CollectionIndexes() = 0;

	virtual void OnCreate() = 0;

	virtual void OnUpdate() = 0;

	static std::string GetCollectionName() 
	{
		return TClass::GetCollectionNameInternal();
	}

	static std::string GetIdentityName() 
	{
		return TClass::GetIdentityNameInternal();
	}
};

template <class TClass>
class DatabaseCollection
{

public:
	static std::string ImplementMe()
	{
		return TClass::ImplementMe_Implemented();
	}

	std::string ImplementMeAlso()
	{
		return "Test";
	}
};

class DatabaseCollectionTest
{

public:

	std::string ImplementMeAsWell()
	{
		return "Test";
	}
};