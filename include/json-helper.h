/*
    __ ____ _____ _____
 __|  |  __|     |   | |  JSON C++ Helper
|  |  |__  |  |  | | | |  Serialization/Deserialization tools
|     |    |     | |   |  Version 0.0.1-beta
|_____|____|_ ___|_|___|

Licensed under the MIT License <http://opensource.org/licenses/MIT>.
SPDX-License-Identifier: MIT
Copyright (c) 2021-2022 RyvalSoft, LLC

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#pragma once

#include "nlohmann/json.hpp"
#include <boost/variant.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/greg_date.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/regex.hpp>

#include <stdlib.h>
#include <map>

#ifdef USING_MONGO_POCO
	#include "Poco/UUID.h"
	#include "Poco/MongoDB/Binary.h"
	#include "Poco/MongoDB/Document.h"
	#include "Poco/UUIDGenerator.h"
#endif

#ifdef USING_MONGO_CXX
	#include <mongocxx/client.hpp>
	#include <bsoncxx/builder/basic/document.hpp>
	#include <bsoncxx/json.hpp>
	#include <bsoncxx/types.hpp>

	using bsoncxx::builder::basic::kvp;
	using bsoncxx::builder::basic::make_document;

	static bsoncxx::types::b_null BSON_NULL_VALUE = bsoncxx::types::b_null{};

#endif

#define HELPER_VARIANTS_TYPES int, double, long, bool, std::string

static boost::uuids::uuid BOOST_NIL_UUID = boost::uuids::nil_uuid();

static const std::string base64_chars =
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			"abcdefghijklmnopqrstuvwxyz"
			"0123456789+/";

const static std::string date_formats[] = {"%Y-%m-%dT%H:%M:%S%f", "%Y-%m-%d %H:%M:%S%f", "%Y-%m-%d %H:%M:%S", "%Y-%m-%d %H:%M"};

#define DB_IDENTITY(TYPE, FIELD) TYPE get_identity() { return FIELD;}

enum EDateFormat
{
	DATE_UNKNOWN,
	DATE_YMD,
	DATE_YMD_HM,
	DATE_SQL,
	DATE_ISO
};

static boost::regex re_ymd("^(\\d{4})-(\\d{2})-(\\d{2})$");
static boost::regex re_iso("^(\\d{4})-(\\d{2})-(\\d{2})T(\\d{2}):(\\d{2}):(\\d{2})\\.(\\d{3})Z$");
static boost::regex re_sql("^(\\d{4})-(\\d{2})-(\\d{2}) (\\d{2}):(\\d{2}):(\\d{2})$");
static boost::regex re_ymd_hm("^(\\d{4})-(\\d{2})-(\\d{2}) (\\d{2}):(\\d{2})$");

struct json_helper
{

	static std::string join_string_array(const std::vector<std::string> array, const std::string sep, const int start=0)
	{
		std::string out;
		for(size_t i=start; i<array.size(); i++)
		{
			if(!out.empty())
			{
				out.append(sep);
			}
			out.append(array.at(i));
		}
		return out;
	}

	static void json_to_map(nlohmann::json data, std::map<std::string, boost::variant<HELPER_VARIANTS_TYPES>> &output)
	{
		for (auto& el : data.items())
		{

			auto value = el.value();
			std::string key = el.key();

			if (el.value().type() == nlohmann::detail::value_t::string)
			{
				output.insert(
						std::pair<std::string, boost::variant<HELPER_VARIANTS_TYPES>>(key, static_cast<std::string>(value))
				);
			}
			else if (el.value().type() == nlohmann::detail::value_t::number_integer)
			{
				output.insert(
						std::pair<std::string, boost::variant<HELPER_VARIANTS_TYPES>>(key, static_cast<int>(value))
				);
			}
			else if (el.value().type() == nlohmann::detail::value_t::number_float)
			{
				output.insert(
						std::pair<std::string, boost::variant<HELPER_VARIANTS_TYPES>>(key, static_cast<double>(value))
				);
			}
			else if (el.value().type() == nlohmann::detail::value_t::boolean)
			{
				output.insert(
						std::pair<std::string, boost::variant<HELPER_VARIANTS_TYPES>>(key, static_cast<bool>(value))
				);
			}
			else if (el.value().type() == nlohmann::detail::value_t::number_unsigned)
			{
				output.insert(
						std::pair<std::string, boost::variant<HELPER_VARIANTS_TYPES>>(el.key(), static_cast<long>(value))
				);
			}
		}
	}

	template <typename T>
	static void set_value(nlohmann::json& data, const std::string json_path, T value)
	{
		std::vector<std::string> path;
		boost::split(path, json_path, boost::is_any_of("."));
		std::string p = path.at(0);

		if(path.size() == 1)
		{
			data[p] = value;
			return;
		}
		else
		{
			std::string joined = join_string_array(path, ".", 1);
			if(!data.contains(p))
			{
				data[p] = {};
			}
			set_value(data[p], joined, value);
		}
	}

	template <typename T>
	static T get_value(const nlohmann::json data, const std::string json_path)
	{
		std::vector<std::string> path;
		boost::split(path, json_path, boost::is_any_of("."));
		std::string p = path.at(0);

		if(path.size() == 1)
		{
			if(data.contains(p))
			{
				return data[p].get<T>();
			}
		}
		else
		{
			std::string joined = join_string_array(path, ".", 1);
			if(data.contains(p))
			{
				return get_value<T>(data[p], joined);
			}
		}

		return {};
	}

	template <typename T, size_t N>
	static void from_json(const nlohmann::json& j, T (&t)[N])
	{
		if (j.size() != N) {
			throw std::runtime_error("JSON array size is different than expected");
		}
		size_t index = 0;
		for (auto& item : j)
		{
			t[index]->from_json(item);
			index++;
		}
	}

	template <typename T>
	static T get_enum(const nlohmann::json& j, std::string field)
	{
		std::string value = get_string(j, field);
		return T::to_enum(value);
	}

	template <typename T>
	static void from_json(const nlohmann::json& j, std::vector<T> &t)
	{
		size_t index = 0;
		for (auto& item : j)
		{
			t.push_back(T{});
			t.at(index).from_json(item);
			index++;
		}
	}

	template <typename T>
	static std::vector<T> get_entity_array(nlohmann::json j, std::string field)
	{
		std::vector<T> data;
		nlohmann::json json_array = j[field];

		from_json(json_array, data);
		return data;
	}

	template <typename T>
	static T get_entity(nlohmann::json j, std::string field)
	{
		T t;
		t.from_json(j[field]);
		return t;
	}

	template <typename T>
	static std::vector<nlohmann::json> to_json_array(std::vector<T> in)
	{
		std::vector<nlohmann::json> data;

		for (auto& item : in)
		{
			data.push_back(item);
		}

		return data;
	}

	template <typename T>
	static nlohmann::json to_json(T& t)
	{
		if (!t)
		{
			return nlohmann::json();
		}

		nlohmann::json j = t->to_json();

		return j;
	}

	static bool get_boolean(const nlohmann::json data, std::string field)
	{
		if(!data.contains(field))
		{
			return false;
		}

		try
		{
			//TODO add more logic for string true/false
			auto value = data[field];
			auto val_type = value.type();
			return static_cast<bool>(value);
		}
		catch(...)
		{
			return false;
		}
	}

	static long get_long(const nlohmann::json data, std::string field)
	{
		if(!data.contains(field))
		{
			return 0;
		}

		try
		{
			//TODO add more logic for string true/false
			auto value = data[field];
			auto val_type = value.type();
			return static_cast<long>(value);
		}
		catch(...)
		{
			return 0;
		}
	}

	static double get_double(const nlohmann::json data, std::string field)
	{
		if(!data.contains(field))
		{
			return 0;
		}

		try
		{
			auto value = data[field];
			auto val_type = value.type();
			return static_cast<double>(value);
		}
		catch(...)
		{
			return 0;
		}
	}

	static float get_float(const nlohmann::json data, std::string field)
	{
		if(!data.contains(field))
		{
			return 0;
		}

		try
		{
			auto value = data[field];
			auto val_type = value.type();
			return static_cast<float>(value);
		}
		catch(...)
		{
			return 0;
		}
	}

	static std::string get_date_str(const nlohmann::json data, std::string field)
	{
		if(!data.contains(field))
		{
			return "";
		}

		try
		{
			auto value = data[field];
			auto val_type = value.type();
			return static_cast<std::string>(value);
		}
		catch(...)
		{
			return "";
		}
	}

	static std::tm parse_datetime(const std::string date_str)
	{
		std::tm tm = {};
		std::istringstream is(date_str);

		if (boost::regex_match(date_str, re_iso)) 
		{
			is >> std::get_time(&tm, "%Y-%m-%dT%H:%M:%S%f");
		}
		else if (boost::regex_match(date_str, re_sql)) 
		{
			is >> std::get_time(&tm, "%Y-%m-%d %H:%M:%S");
		}
		else if (boost::regex_match(date_str, re_ymd)) 
		{
			is >> std::get_time(&tm, "%Y-%m-%d");
		}
		else if (boost::regex_match(date_str, re_ymd_hm)) 
		{
			is >> std::get_time(&tm, "%Y-%m-%d %H:%M");
		}

		return tm;
	}

	static boost::posix_time::ptime get_date(std::string date_str)
	{
		std::tm tm = parse_datetime(date_str);
		boost::posix_time::ptime t = boost::posix_time::ptime_from_tm(tm); //convert tm to  ptime
		return t;
	}

	static boost::posix_time::ptime parse_date(std::tm tm)
	{
		boost::posix_time::ptime t = boost::posix_time::ptime_from_tm(tm); //convert tm to  ptime
		return t;
	}

	static boost::posix_time::ptime get_date(const nlohmann::json data, std::string field)
	{
		try
		{
			auto value = data[field];
			auto val_type = value.type();
			static const boost::posix_time::ptime epoch(boost::gregorian::date(1970, 1, 1));
			if (val_type == nlohmann::detail::value_t::string)
			{
				std::string date_str = get_string(data, field);
				std::tm tm = parse_datetime(date_str); // Try to parse date
				boost::posix_time::ptime t = boost::posix_time::ptime_from_tm(tm); //convert tm to  ptime
				return t;
			}
			else if (val_type == nlohmann::detail::value_t::number_integer)
			{
				int64_t date_int64 = static_cast<long>(value);
				boost::posix_time::milliseconds ms(date_int64);
				static const boost::posix_time::ptime epoch(boost::gregorian::date(1970, 1, 1));
				boost::posix_time::ptime t(epoch + ms);
				return t;
			}

			return boost::posix_time::ptime();
		}
		catch (std::exception const& ex)
		{
			std::cout << "Error getting value: " << ex.what() << std::endl;
			return boost::posix_time::ptime();
		}
	}

	static bool is_base64(unsigned char c)
	{
		return (isalnum(c) || (c == '+') || (c == '/'));
	}

	static std::string base64_decode(std::string const &encoded_string)
	{
		size_t in_len = encoded_string.size();
		size_t i = 0;
		size_t j = 0;
		int in_ = 0;
		unsigned char char_array_4[4], char_array_3[3];
		std::string ret;

		while (in_len-- && (encoded_string[in_] != '=') && is_base64(encoded_string[in_]))
		{
			char_array_4[i++] = encoded_string[in_];
			in_++;
			if (i == 4)
			{
				for (i = 0; i < 4; i++)
				{
					char_array_4[i] = static_cast<unsigned char>(base64_chars.find(char_array_4[i]));
				}

				char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
				char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
				char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

				for (i = 0; (i < 3); i++)
				{
					ret += char_array_3[i];
				}
				i = 0;
			}
		}

		if (i)
		{
			for (j = i; j < 4; j++)
			{
				char_array_4[j] = 0;
			}

			for (j = 0; j < 4; j++)
			{
				char_array_4[j] = static_cast<unsigned char>(base64_chars.find(char_array_4[j]));
			}

			char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
			char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
			char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

			for (j = 0; (j < i - 1); j++)
			{ ret += char_array_3[j]; }
		}

		return ret;
	}

	static boost::uuids::uuid get_uuid(nlohmann::json data, const std::string field)
	{
		if(!data.contains(field))
		{
			return boost::uuids::nil_uuid();
		}

		auto value = data[field];
		auto val_type = value.type();

		try
		{
			if (val_type == nlohmann::detail::value_t::string)
			{
				std::string uuid_string = data[field].get<std::string>();
				return boost::lexical_cast<boost::uuids::uuid>(uuid_string);
			}
			else if (val_type == nlohmann::detail::value_t::object)
			{
				std::string uuid_string = value.get<std::string>();
				return boost::lexical_cast<boost::uuids::uuid>(value);
			}
		}
		catch (std::exception const& ex)
		{
			std::cout << "Error getting value: " << ex.what() << std::endl;
		}

		return boost::uuids::nil_uuid();
	}

	static std::string get_string(nlohmann::json data, const std::string field)
	{
		if(!data.contains(field))
		{
			return "";
		}

		auto value = data[field];
		auto val_type = value.type();

		try
		{
			if (val_type == nlohmann::detail::value_t::string)
			{
				return value;
			}
			else if (val_type == nlohmann::detail::value_t::number_integer)
			{
				return std::to_string(static_cast<long>(value));
			}
			else if (val_type == nlohmann::detail::value_t::number_float)
			{
				return std::to_string(static_cast<double>(value));
			}
			else if (val_type == nlohmann::detail::value_t::boolean)
			{
				return (static_cast<bool>(value)) ? "true" : "false";
			}
		}
		catch(...)
		{}

		return "";
	}

	static int get_integer(nlohmann::json data, const std::string field)
	{
		if(!data.contains(field))
		{
			return 0;
		}

		auto value = data[field];
		auto val_type = value.type();

		try
		{
			if (val_type == nlohmann::detail::value_t::number_integer)
			{
				return value;
			}
			else if (val_type == nlohmann::detail::value_t::number_unsigned)
			{
				return value;
			}
		}
		catch(...)
		{}

		return 0;
	}

	static double get_number(nlohmann::json data, const std::string field)
	{
		if(!data.contains(field))
		{
			return 0;
		}

		auto value = data[field];
		auto val_type = value.type();

		try
		{
			if (val_type == nlohmann::detail::value_t::number_integer)
			{
				return value;
			}
			else if (val_type == nlohmann::detail::value_t::number_float)
			{
				return static_cast<double>(value);
			}
		}
		catch(...)
		{}

		return 0;
	}

	static std::string to_string(boost::uuids::uuid value)
	{
		return boost::lexical_cast<std::string>(value);
	}

	static std::string to_string(boost::posix_time::ptime value, const char* format)
	{
		char date_time[64]="";

		try
		{
			//todo Error C4996: 'sprintf': This function or variable may be unsafe. Consider using sprintf_s instead.
			sprintf(date_time,
				((format) ? format : "%04i-%02i-%02i %02i:%02i:%02i"),
				static_cast<int>(value.date().year()),
				static_cast<int>(value.date().month()),
				static_cast<int>(value.date().day()),
				static_cast<int>(value.time_of_day().hours()),
				static_cast<int>(value.time_of_day().minutes()),
				static_cast<int>(value.time_of_day().seconds())
			);
		}
		catch (...)
		{

		}

		return date_time;
	}

	static std::string to_string(boost::posix_time::ptime value)
	{
		return to_string(value, nullptr);
	}

	static std::string to_mongo_type(std::string value)
	{
		return value;
	}

	static long to_mongo_type(long value)
	{
		return value;
	}

	static int to_mongo_type(int value)
	{
		return value;
	}

	static double to_mongo_type(double value)
	{
		return value;
	}

	static float to_mongo_type(float value)
	{
		return value;
	}

	static bool to_mongo_type(bool value)
	{
		return value;
	}



#ifdef USING_MONGO_CXX

	static bsoncxx::types::b_date to_mongo_type(boost::posix_time::ptime value)
	{
			return to_date(value);
	}

	//Converts for template generic classes
	static bsoncxx::types::b_binary to_mongo_type(boost::uuids::uuid value)
	{
		return to_b_uuid(value);
	}

	template <typename T>
	static T get_enum(bsoncxx::document::view doc, std::string field)
	{
		std::string value = get_string(doc, field);
		return T::to_enum(value);
	}

	template <typename T>
	static T get_entity(bsoncxx::document::view doc, std::string field)
	{
		T t;
		bsoncxx::document::element element_f = doc[field];
		t.from_view(element_f.get_document().view());
		return t;
	}

	static bool get_boolean(bsoncxx::document::view doc, std::string field)
	{
		bsoncxx::document::element element_f = doc[field];

		if (!element_f)
		{
			return false;
		}

		if (element_f.type() == bsoncxx::type::k_utf8)
		{
			std::string value = element_f.get_utf8().value.to_string();
			return value.compare("true") == 0 || value.compare("TRUE") == 0;
		}
		else if (element_f.type() == bsoncxx::type::k_bool)
		{
			return element_f.get_bool();
		}

		return false;
	}

	static std::string get_string(bsoncxx::document::view doc, std::string field)
	{
		bsoncxx::document::element element_f = doc[field];

		if (!element_f)
		{
			return "";
		}

		if (element_f.type() == bsoncxx::type::k_utf8)
		{
			return element_f.get_utf8().value.to_string();
		}
		else if (element_f.type() == bsoncxx::type::k_double)
		{
			return std::to_string(element_f.get_double());
		}
		else if (element_f.type() == bsoncxx::type::k_int32)
		{
			return std::to_string(element_f.get_int32());
		}
		else if (element_f.type() == bsoncxx::type::k_int64)
		{
			return std::to_string(element_f.get_int64());
		}
		else if (element_f.type() == bsoncxx::type::k_bool)
		{
			return (element_f.get_bool()) ? "true" : "false";
		}

		return "";

	}

	static boost::posix_time::ptime get_date(bsoncxx::document::view doc, std::string field)
	{
		bsoncxx::document::element element_f = doc[field];

		if (!element_f || element_f.type() != bsoncxx::type::k_date)
		{
			return boost::posix_time::ptime();
		}

		int64_t date_int64 = element_f.get_date().to_int64();

		boost::posix_time::milliseconds ms(date_int64);

		static const boost::posix_time::ptime epoch(boost::gregorian::date(1970, 1, 1));

		boost::posix_time::ptime t(epoch + ms);

		return t;
	}

	static void get_date_str(bsoncxx::document::view doc, std::string field, char* date_time, const char* format)
	{
		boost::posix_time::ptime t = get_date(doc, field);

		sprintf(date_time,
			((format) ? format : "%04i-%02i-%02i %02i:%02i:%02i"),
			static_cast<int>(t.date().year()),
			static_cast<int>(t.date().month()),
			static_cast<int>(t.date().day()),
			static_cast<int>(t.time_of_day().hours()),
			static_cast<int>(t.time_of_day().minutes()),
			static_cast<int>(t.time_of_day().seconds())
		);
	}

	static std::string get_date_str(bsoncxx::document::view doc, std::string field, const char* format)
	{
		boost::posix_time::ptime t = get_date(doc, field);

		char date_time[64] = "";

		sprintf(date_time,
			((format) ? format : "%04i-%02i-%02i %02i:%02i:%02i"),
			static_cast<int>(t.date().year()),
			static_cast<int>(t.date().month()),
			static_cast<int>(t.date().day()),
			static_cast<int>(t.time_of_day().hours()),
			static_cast<int>(t.time_of_day().minutes()),
			static_cast<int>(t.time_of_day().seconds())
		);

		return std::string(date_time);
	}

	static std::string get_date_str(bsoncxx::document::view doc, std::string field)
	{
		return get_date_str(doc, field, nullptr);
	}

	static void get_map(bsoncxx::document::view doc, std::string field, std::map<std::string, boost::variant<HELPER_VARIANTS_TYPES>>& output)
	{
		auto SubField = doc[field];

		if(!SubField)
		{
			return;
		}

		bsoncxx::document::view subdoc = SubField.get_document();

		for (const auto& element : subdoc)
		{
			std::string key = static_cast<std::string>(element.key());

			if(element.type() == bsoncxx::type::k_double)
			{
				output.insert(
						std::pair<std::string, boost::variant<HELPER_VARIANTS_TYPES>>(key, static_cast<double>(element.get_double()))
				);
			}
			else if(element.type() == bsoncxx::type::k_int32)
			{
				output.insert(
						std::pair<std::string, boost::variant<HELPER_VARIANTS_TYPES>>(key, static_cast<int>(element.get_int32()))
				);
			}
			else if(element.type() == bsoncxx::type::k_int64)
			{
				output.insert(
						std::pair<std::string, boost::variant<HELPER_VARIANTS_TYPES>>(key, static_cast<long>(element.get_int64()))
				);
			}
			else if(element.type() == bsoncxx::type::k_utf8)
			{
				output.insert(
						std::pair<std::string, boost::variant<HELPER_VARIANTS_TYPES>>(key, static_cast<std::string>(element.get_utf8().value.to_string()))
				);
			}
			else if(element.type() == bsoncxx::type::k_bool)
			{
				output.insert(
						std::pair<std::string, boost::variant<HELPER_VARIANTS_TYPES>>(key, static_cast<bool>(element.get_bool()))
				);
			}
			else
			{
				std::cout << "invalid json type for field" << key << std::endl;
			}
		}

	}

	static long get_long(bsoncxx::document::view doc, const std::string field)
	{
		bsoncxx::document::element element_f = doc[field];

		if (!element_f)
		{
			return 0;
		}

		return (element_f.type() == bsoncxx::type::k_int64) ? element_f.get_int64() : 0;
	}

	static int get_integer(bsoncxx::document::view doc, const std::string field)
	{
		bsoncxx::document::element element_f = doc[field];

		if (!element_f)
		{
			return 0;
		}

		return (element_f.type() == bsoncxx::type::k_int32) ? element_f.get_int32() : 0;
	}

	static double get_double(bsoncxx::document::view doc, const std::string field)
	{
		bsoncxx::document::element element_f = doc[field];

		if (!element_f)
		{
			return 0;
		}

		return (element_f.type() == bsoncxx::type::k_double) ? element_f.get_double() : 0.0;
	}

	static std::tuple<std::string, bsoncxx::types::b_binary> uuid_kvp(std::string field, boost::uuids::uuid uuid)
	{
		return kvp(field, bsoncxx::types::b_binary {
			bsoncxx::binary_sub_type::k_uuid, 
			16, 
			uuid.data
		});
	}

	static bsoncxx::types::b_binary to_b_uuid(boost::uuids::uuid& uuid)
	{
		bsoncxx::types::b_binary out = bsoncxx::types::b_binary {
			bsoncxx::binary_sub_type::k_uuid, 
			16,
			(const uint8_t*)&uuid
		};

		return out;
	}

	static void to_b_uuid(boost::uuids::uuid& uuid, bsoncxx::types::b_binary& out)
	{
		out = bsoncxx::types::b_binary {
				bsoncxx::binary_sub_type::k_uuid,
				16,
				(const uint8_t*)&uuid
		};
	}

	static bsoncxx::types::b_date to_date(boost::posix_time::ptime date)
	{
		time_t time = to_time_t(date);

		return bsoncxx::types::b_date( 
			std::chrono::system_clock::from_time_t(time)		
		);
	}

	static boost::uuids::uuid get_uuid(bsoncxx::types::b_binary uuid_binary)
	{
		try
		{
			const uint8_t* d_bytes = uuid_binary.bytes;
			const int d_bytes_size = uuid_binary.size;

			boost::uuids::uuid u;
			std::vector<uint8_t> v;

			for (int i = 0; i < d_bytes_size; i++)
			{
				v.push_back(d_bytes[i]);
			}

			std::copy(v.begin(), v.end(), u.begin());
			return u;

		}
		catch (std::exception const& ex)
		{
			std::cout << "Error getting value: " << ex.what() << std::endl;
		}

		return boost::uuids::nil_uuid();
	}

	static boost::uuids::uuid get_uuid(bsoncxx::document::view doc, const std::string field)
	{
		bsoncxx::document::element uuid_ele = doc[field];

		if(!uuid_ele)
		{
			return boost::uuids::uuid();
		}

		try
		{
			if (uuid_ele.type() == bsoncxx::type::k_binary)
			{
				bsoncxx::types::b_binary uuid_binary = uuid_ele.get_binary();

				const uint8_t* d_bytes = uuid_binary.bytes;
				const int d_bytes_size = uuid_binary.size;

				boost::uuids::uuid u;
				std::vector<uint8_t> v;

				for (int i = 0; i < d_bytes_size; i++)
				{
					v.push_back(d_bytes[i]);
				}

				std::copy(v.begin(), v.end(), u.begin());
				return u;
			}
		}
		catch (std::exception const& ex)
		{
			std::cout << "Error getting value: " << ex.what() << std::endl;
		}

		return boost::uuids::nil_uuid();
	}

	static bsoncxx::document::value create_document(nlohmann::json Data)
	{
		bsoncxx::builder::basic::document basic_builder{};

		for (auto& el : Data.items())
		{
			std::string key = el.key();
			auto value = el.value();
			auto val_type = el.value().type();

			if (val_type == nlohmann::detail::value_t::string)
			{
				basic_builder.append(kvp(
					key,
					static_cast<std::string>(value)
				));
			}
			else if (val_type == nlohmann::detail::value_t::number_integer)
			{
				basic_builder.append(kvp(
					key,
					static_cast<long>(value)
				));
			}
			else if (val_type == nlohmann::detail::value_t::number_float)
			{
				basic_builder.append(kvp(
					key,
					static_cast<double>(value)
				));
			}
			else if (val_type == nlohmann::detail::value_t::boolean)
			{
				basic_builder.append(kvp(
					key,
					static_cast<bool>(value)
				));
			}

		}

		return basic_builder.extract();
	}

	static bsoncxx::document::value map_to_document(std::map<std::string, boost::variant<HELPER_VARIANTS_TYPES>> data)
	{
		bsoncxx::builder::basic::document doc{};
		std::map<std::string, boost::variant<HELPER_VARIANTS_TYPES>>::iterator it;

		for (it = data.begin(); it != data.end(); it++)
		{
			if(it->second.type() == typeid(std::string))
			{
				doc.append(
						kvp(it->first, boost::get<std::string>(it->second))
				);
			}
			else if(it->second.type() == typeid(int))
			{
				doc.append(
						kvp(it->first, boost::get<int>(it->second))
				);
			}
			else if(it->second.type() == typeid(long))
			{
				doc.append(
						kvp(it->first, boost::get<long>(it->second))
				);
			}
			else if(it->second.type() == typeid(bool))
			{
				doc.append(
						kvp(it->first, boost::get<bool>(it->second))
				);
			}
			else if(it->second.type() == typeid(double))
			{
				doc.append(
						kvp(it->first, boost::get<double>(it->second))
				);
			}
			else
			{
				std::cout << "invalid typeid for field" << it->first << std::endl;
			}
		}

		return doc.extract();

	}

#endif

#if defined(USING_MONGO_POCO)
	template <typename T>
	static T get_enum(Poco::MongoDB::Document doc, std::string field)
	{
		std::string value = get_string(doc, field);
		return T::to_enum(value);
	}

	template <typename T>
	static T get_entity(Poco::MongoDB::Document doc, std::string field)
	{
		T t;

		if (!doc.exists(field))
		{
			return t;
		}

		Poco::MongoDB::Document* subdoc = doc.get<Poco::MongoDB::Document::Ptr>(field).get();
		t.from_database(*subdoc);
		return t;
	}

	static bool get_boolean(Poco::MongoDB::Document doc, std::string field)
	{
		if (!doc.exists(field))
		{
			return false;
		}

		Poco::MongoDB::Element::Ptr element_f = doc.get(field);

		if(Poco::MongoDB::ElementTraits<std::string>::TypeId == element_f->type())
		{
			std::string value = doc.get<std::string>(field);
			return value.compare("true") == 0 || value.compare("TRUE") == 0;
		}
		else if(Poco::MongoDB::ElementTraits<double>::TypeId == element_f->type())
		{
			return doc.get<bool>(field);
		}

		return false;
	}

	static std::string get_string(Poco::MongoDB::Document doc, std::string field)
	{
		if (!doc.exists(field))
		{
			return "";
		}

		Poco::MongoDB::Element::Ptr element_f = doc.get(field);

		if(Poco::MongoDB::ElementTraits<std::string>::TypeId == element_f->type())
		{
			return doc.get<std::string>(field);
		}
		else if(Poco::MongoDB::ElementTraits<double>::TypeId == element_f->type())
		{
			double value = doc.get<double>(field);
			return std::to_string(value);
		}
		else if(Poco::MongoDB::ElementTraits<Poco::Int32>::TypeId == element_f->type())
		{
			std::int32_t value = doc.get<Poco::Int32>(field);
			return std::to_string(value);
		}
		else if(Poco::MongoDB::ElementTraits<Poco::Int64>::TypeId == element_f->type())
		{
			std::int64_t value = doc.get<Poco::Int64>(field);
			return std::to_string(value);
		}
		else if(Poco::MongoDB::ElementTraits<bool>::TypeId == element_f->type())
		{
			return (doc.get<bool>(field)) ? "true" : "false";
		}

		return "";
	}

	static boost::posix_time::ptime get_date(Poco::MongoDB::Document doc, std::string field)
	{
		if (!doc.exists(field))
		{
			return boost::posix_time::ptime();
		}

		Poco::Timestamp ts_value = doc.get<Poco::Timestamp>(field);
		boost::posix_time::ptime t = boost::posix_time::from_time_t(ts_value.epochTime());

		return t;
	}

	static void get_date_str(Poco::MongoDB::Document doc, std::string field, char* date_time, const char* format)
	{
		boost::posix_time::ptime t = get_date(doc, field);

		sprintf(date_time,
				((format) ? format : "%04i-%02i-%02i %02i:%02i:%02i"),
				static_cast<int>(t.date().year()),
				static_cast<int>(t.date().month()),
				static_cast<int>(t.date().day()),
				static_cast<int>(t.time_of_day().hours()),
				static_cast<int>(t.time_of_day().minutes()),
				static_cast<int>(t.time_of_day().seconds())
		);
	}

	static std::string get_date_str(Poco::MongoDB::Document doc, std::string field, const char* format)
	{
		boost::posix_time::ptime t = get_date(doc, field);

		char date_time[64] = "";

		sprintf(date_time,
				((format) ? format : "%04i-%02i-%02i %02i:%02i:%02i"),
				static_cast<int>(t.date().year()),
				static_cast<int>(t.date().month()),
				static_cast<int>(t.date().day()),
				static_cast<int>(t.time_of_day().hours()),
				static_cast<int>(t.time_of_day().minutes()),
				static_cast<int>(t.time_of_day().seconds())
		);

		return std::string(date_time);
	}

	static std::string get_date_str(Poco::MongoDB::Document doc, std::string field)
	{
		return get_date_str(doc, field, nullptr);
	}

	static long get_long(Poco::MongoDB::Document doc, const std::string field)
	{
		if (!doc.exists(field))
		{
			return 0;
		}

		return (Poco::MongoDB::ElementTraits<Poco::Int64>::TypeId == doc.get(field)->type()) ? doc.get<Poco::Int64>(field) : 0;
	}

	static int get_integer(Poco::MongoDB::Document doc, const std::string field)
	{
		if (!doc.exists(field))
		{
			return 0;
		}

		return (Poco::MongoDB::ElementTraits<Poco::Int64>::TypeId == doc.get(field)->type()) ? doc.get<Poco::Int32>(field) : 0;
	}

	static double get_double(Poco::MongoDB::Document doc, const std::string field)
	{
		if (!doc.exists(field))
		{
			return 0.f;
		}

		return (Poco::MongoDB::ElementTraits<Poco::Int64>::TypeId == doc.get(field)->type()) ? doc.get<double>(field) : 0.f;
	}

	static Poco::MongoDB::Binary::Ptr to_b_uuid(boost::uuids::uuid value)
	{
		std::string uuid_str = boost::lexical_cast<std::string>(value);

		Poco::MongoDB::Binary::Ptr uuid_binary = new Poco::MongoDB::Binary(
				Poco::UUID(uuid_str)
		);

		return uuid_binary;
	}

	static Poco::Timestamp to_date(boost::posix_time::ptime date)
	{
		time_t time = to_time_t(date);
		return Poco::Timestamp::fromEpochTime(time);
	}

	static Poco::Int64 to_int64(long value)
	{
		return static_cast<Poco::Int64>(value);
	}

	static Poco::Int32 to_int32(int value)
	{
		return static_cast<Poco::Int32>(value);
	}

	static boost::uuids::uuid get_uuid(Poco::MongoDB::Document doc, const std::string field)
	{
		if (!doc.exists(field))
		{
			return boost::uuids::nil_uuid();
		}

		Poco::MongoDB::Element::Ptr element_f = doc.get(field);

		try
		{
			if(Poco::MongoDB::ElementTraits<Poco::MongoDB::Binary::Ptr>::TypeId == element_f->type())
			{
				Poco::MongoDB::Binary* uuid_binary = doc.get<Poco::MongoDB::Binary::Ptr>(field);
				return boost::lexical_cast<boost::uuids::uuid>(uuid_binary->uuid().toString());
			}
		}
		catch (std::exception const& ex)
		{
			std::cout << "Error getting value: " << ex.what() << std::endl;
		}

		return boost::uuids::nil_uuid();
	}

	template <typename T>
	static Poco::MongoDB::Document::Ptr make_reference_doc(std::string field, T value)
	{
		Poco::MongoDB::Document::Ptr doc = new Poco::MongoDB::Document();
		doc->add(field, value);
		return doc;
	}



#endif

	static nlohmann::json map_to_json(std::map<std::string, boost::variant<HELPER_VARIANTS_TYPES>> data)
	{
		nlohmann::json output;
		std::map<std::string, boost::variant<HELPER_VARIANTS_TYPES>>::iterator it;

		for (it = data.begin(); it != data.end(); it++)
		{
			if(it->second.type() == typeid(std::string))
			{
				output[it->first] = boost::get<std::string>(it->second);
			}
			else if(it->second.type() == typeid(int))
			{
				output[it->first] = boost::get<int>(it->second);
			}
			else if(it->second.type() == typeid(long))
			{
				output[it->first] = boost::get<long>(it->second);
			}
			else if(it->second.type() == typeid(bool))
			{
				output[it->first] = boost::get<bool>(it->second);
			}
			else if(it->second.type() == typeid(double ))
			{
				output[it->first] = boost::get<double>(it->second);
			}
			else
			{
				std::cout << "invalid typeid for field" << it->first << std::endl;
			}
		}

		return output;

	}

};
